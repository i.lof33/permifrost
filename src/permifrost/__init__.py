# Managed by bumpversion
__version__ = "0.13.1"

from permifrost.error import SpecLoadingError

__all__ = [
    "SpecLoadingError",
]
